import {Component, EventEmitter, Input, OnChanges, SimpleChange, SimpleChanges} from '@angular/core';
import {ITableRowCellSettings} from '../interfaces/ITableRowCellSettings';
import {IActionColumnSettings} from '../interfaces/IActionColumnSettings';
import {ITableResponse} from "../../../interfaces/ITableResponse";

@Component({
  selector: 'app-table-row',
  templateUrl: './table-row.component.html',
  styleUrls: ['./table-row.component.scss']
})
export class TableRowComponent implements OnChanges {
  @Input() row: any;
  @Input() settings: ITableRowCellSettings;
  @Input() actionColumnSettings?: IActionColumnSettings;
  @Input() events: EventEmitter<ITableResponse>;
  cells = [];
  expandRows = false;
  constructor() { }

  toggleExpand(event) {
    this.expandRows = event;
  }

  // TODO: move to parent component
  makeSettings(settings, data) {
    this.cells = [];
    settings.titles.forEach((title, index) => {
      title = Object.getOwnPropertyNames(title)[0];
      if (settings)
      this.cells.push({
        shadowed: settings.cells[index].shadowed,
        flexGrow: settings.flexGrow,
        title: title,
        value: data[title],
        width: settings.cells[index].width,
        content: settings.cells[index].content,
        flexBasis: settings.cells[index].flexBasis,
        events: settings.cells[index].events,
        order: settings.cells[index].order,
      });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const settings: SimpleChange = changes.settings;
    const data: SimpleChange = changes.row;
    settings ? this.makeSettings(settings.currentValue, this.row) : null;
    data ? this.makeSettings(this.settings, data.currentValue) : null;
  }
}
