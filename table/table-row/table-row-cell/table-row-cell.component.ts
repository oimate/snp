import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import {ViewChild} from '@angular/core';
import {OnChanges} from '@angular/core';

@Component({
  selector: 'app-table-row-cell',
  templateUrl: './table-row-cell.component.html',
  styleUrls: ['./table-row-cell.component.scss']
})
export class TableRowCellComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Output() expand: EventEmitter<boolean> = new EventEmitter();

  expandable = false;
  expanded = false;
  content = 'text';
  cellValue: any;
  @ViewChild('valueElem') valueElem: ElementRef;
  @ViewChild('valueContainer') valueContainer: ElementRef;

  // TODO events
  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  fExpand() {
    this.expanded = !this.expanded;
    this.expand.emit(this.expanded);
  }

  setOrder(order) {
    this.renderer.setStyle(this.elRef.nativeElement, 'order', order);
  }

  setValue(value) {
    this.cellValue = value
  }

  ngOnInit() {
    if (this.data.flexGrow) {
      this.renderer.setStyle(this.elRef.nativeElement, 'flex-basis', this.data.flexBasis);
      this.renderer.setStyle(this.elRef.nativeElement, 'min-width', this.data.flexBasis);
    } else {
      this.renderer.setStyle(this.elRef.nativeElement, 'width', this.data.width);
      this.renderer.setStyle(this.elRef.nativeElement, 'min-width', this.data.width);
    }
    if (this.data.order) {
      this.setOrder(this.data.order)
    }

    if (this.cellValue === 'ld-slave') {
      this.expandable = true;
      this.data.content = 'text-start';
      this.cellValue = 'Все';
    }

    switch (this.data.content) {
      case 'numeric':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'img':
        // TODO
        break;
      case 'text-start':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'text':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-end');
        break;
      case 'array-text':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-end');
        break;
      case 'link':
        this.renderer.addClass(this.valueContainer.nativeElement, 'table-link');
        break;
      case 'date':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'master':
        this.expandable = true;
        break;
      case 'money':
        this.renderer.setStyle(this.valueContainer.nativeElement, 'justify-content', 'flex-start');
        break;
    }
  }

  match(): boolean {
    return /^http[s]?:\/\//.test(this.cellValue.link);
  }

  ngOnChanges(changes: SimpleChanges) {
    const data: SimpleChange = changes.data;
    if (data)  {
      if (data.currentValue.content === 'link' && typeof data.currentValue.value === 'string'
        && data.currentValue.value !== 'ld-slave') {
        this.setOrder(data.currentValue.order);
        this.cellValue = {
          link: data.currentValue.value,
          name: data.currentValue.value
        };
      } else {
        this.setOrder(data.currentValue.order);
        this.setValue(data.currentValue.value);
      }
    }
  }
}
