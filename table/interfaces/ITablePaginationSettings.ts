export interface ITablePaginationSettings {
  perPage: number|string;
  total: number;
  totalElements: number;
  currentPage: number;
  perPages: Array<number|string>;
}
