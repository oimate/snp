export interface ITableRowCellSettings {
  titles: Array<any>;
  cells: Array<any>;

  actionColumn?: any;
  flexGrow: boolean;
}
