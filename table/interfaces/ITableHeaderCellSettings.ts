export interface ITableHeaderCellSettings {
  width: number|string;
  sort: boolean;
  filter: boolean;
  flexGrow: boolean;
  flexBasis: string;
  content: string;
  order?: number;
  sortDir?: 'asc' | 'desc' | null
}
