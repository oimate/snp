export interface IActionColumnSettings {
  width: number|string;
  flexBasis: number|string;
  direction: string;
  actions: Array<{
    class: string;
    link: string;
    type: 'trigger'|'link';
    text: string;
  }>;
}
