import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {ITablePaginationSettings} from '../interfaces/ITablePaginationSettings';
import {EventEmitter} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {ITableResponse} from '../../../interfaces/ITableResponse';
import {SimpleChange} from '@angular/core';
import {MediaInvestigator} from "../../../models/MediaInvestigator";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.scss']
})
export class TablePaginationComponent implements OnChanges, OnDestroy {

  @Input() settings$: ITablePaginationSettings;
  @Input() events?: EventEmitter<ITableResponse>;
  investigator; currentState; mediaSub;
  constructor() {
    this.investigator = new MediaInvestigator(fromEvent(window, 'resize').pipe(debounceTime(500)));
    this.mediaSub = this.investigator.currentState.subscribe(cs => {
      this.currentState = cs;
      if (cs === 'mobile') {
        this.maxShown = 5;
      }
      if (cs === 'tablet') {
        this.maxShown = 7;
      }
    });
  }
  settings;
  paginationCells = [];
  maxShown = 9;
  perPage: number|string;
  currentPage: number;
  totalElements: number;
  sub: Subscription;
  changePage(page) {
    if (!(page === this.currentPage)) {
      this.currentPage = page;
      this.paginate();
    }
  }

  changePerPage(perPage) {
    this.perPage = perPage;
    this.paginate();
  }

  paginate() {
    this.events.emit({
      type: 'pagination',
      value: {
        page: this.currentPage,
        perPage: this.perPage
      }
    });
  }

  makePaginationCells() {
    // 1 2 ... 4 5 6 ... 8 9
    // количество боковых ссылок (2 в примере выше)
    let sideLength;
    // количество центральных ссылок (3 в примере выше)
    let middleLength;
    // кейс-переменная.
    // 1 2 3 (4) 5 6 .. 10 11 - кейс 0
    // 1 2 ... 5 (6) 7 .. 10 11 - кейс 1
    // 1 2 ... 6 7 (8) 9 10 11 - кейс 2
    let mode;
    // кол-во боковых ссылок будет: floor(maxShown/3) - 1; один вычитается для '...' ссылки; и приоритетом будет
    // центральная часть - поэтому floor
    Number.isInteger(this.maxShown / 3) ?
      sideLength = this.maxShown / 3 - 1
      : sideLength = Math.floor(this.maxShown / 3);
    // центральная часть - все, кроме крайних и троеточий.
    middleLength = this.maxShown - (sideLength + 1) * 2;
    // величина, в зависимости от которой центральная часть склеивается с одной их крайних. в случаях  если
    // currentPage <= glueLength - case: mode == 0
    // currentPage >= maxShown - glueLength - case: mode == 2
    let glueLength = sideLength + middleLength + 1;
    if ((this.settings.currentPage >= glueLength
         && this.settings.currentPage <= this.settings.total - glueLength + 1)
      ||
        (this.settings.currentPage == glueLength
         && this.settings.currentPage == this.settings.total - glueLength + 1)
    ) {
      if (middleLength == 1 && this.settings.currentPage == glueLength) {
        mode = 0;
      } else {
        if (middleLength == 1 && this.settings.currentPage == this.settings.total - glueLength + 1) {
          mode = 2;
        } else {
          mode = 1;
        }
      }
    } else {
      if (this.settings.currentPage < glueLength) {
        mode = 0;
      } else {
        mode = 2;
      }
    }
    switch(mode) {
      case 0:
        for (let cell = 1; cell <= glueLength; cell++) {
          // page - значение, которое в конечном итоге уйдет в запрос к API.
          // show - то, что будет показано пользователю. равно либо значению page либо '...'
          this.paginationCells.push({page: cell, show: `${cell}`});
        }
        this.paginationCells.push({
          page: Math.floor((this.settings.total - (sideLength - 1) + glueLength)/2),
          show: '...'});
        for (let cell = sideLength - 1; cell >= 0; --cell) {
          this.paginationCells.push({page: this.settings.total - cell, show: `${this.settings.total - cell}`});
        }
        break;
      case 1:
        for (let cell = 1; cell <= sideLength; cell++) {
          this.paginationCells.push({page: cell, show: `${cell}`});
        }
        this.paginationCells.push({
          page: Math.floor((sideLength + this.settings.currentPage - Math.floor(middleLength/2))/2),
          show: '...'});
        for (let cell = 0; cell < middleLength; cell++) {
          this.paginationCells.push({
            page: this.settings.currentPage - Math.floor(middleLength/2) + cell,
            show: `${this.settings.currentPage - Math.floor(middleLength/2) + cell}`});
        }
        this.paginationCells.push({
          page: Math.floor((this.settings.total - sideLength + this.settings.currentPage + Math.floor(middleLength/2))/2),
          show: '...'});
        for (let cell = sideLength - 1; cell >= 0; --cell) {
          this.paginationCells.push({page: this.settings.total - cell, show: `${this.settings.total - cell}`});
        }
        break;
      case 2:
        for (let cell = 1; cell <= sideLength; cell++) {
          this.paginationCells.push({page: cell, show: `${cell}`});
        }
        this.paginationCells.push({
          page: Math.floor((this.settings.total - glueLength + sideLength)/2),
          show: '...'});
        for (let cell = glueLength - 1; cell >= 0; --cell) {
          this.paginationCells.push({page: this.settings.total - cell, show: `${this.settings.total - cell}`});
        }
        break;
    }
  }

  makeSettings(settings) {
    this.settings = settings;
    this.currentPage = settings.currentPage;
    this.totalElements = settings.totalElements;
    this.perPage = settings.perPage;
    this.paginationCells = [];
    if (settings.total > this.maxShown) {
      this.makePaginationCells();
    } else {
      for (let cell = 1; cell <= settings.total; cell++) {
        this.paginationCells.push({page: cell, show: `${cell}`});
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const settings: SimpleChange = changes.settings$;
    this.makeSettings(settings.currentValue);
  }

  ngOnDestroy () {
    this.mediaSub.unsubscribe();
    this.investigator.destroy();
  }

}
