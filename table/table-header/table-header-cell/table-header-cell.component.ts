import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ITableResponse} from '../../../../interfaces/ITableResponse';
import {EventEmitter} from '@angular/core';
import {ITableHeaderCellSettings} from '../../interfaces/ITableHeaderCellSettings';

@Component({
  selector: 'app-table-header-cell',
  templateUrl: './table-header-cell.component.html',
  styleUrls: ['./table-header-cell.component.scss']
})
export class TableHeaderCellComponent implements OnInit {

  @Input() events?: EventEmitter<ITableResponse>;
  @Input() value: any;
  @Input() settings: ITableHeaderCellSettings;
  apiName: string;
  val: string;
  @ViewChild('titleContainer') titleContainer: ElementRef;
  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  setStyles() {
    if (this.settings.flexGrow) {
      this.renderer.setStyle(this.elRef.nativeElement, 'flex-grow', this.settings.width);
      this.renderer.setStyle(this.elRef.nativeElement, 'flex-basis', this.settings.flexBasis);
      this.renderer.setStyle(this.elRef.nativeElement, 'min-width', this.settings.flexBasis);
    } else {
      this.renderer.setStyle(this.elRef.nativeElement, 'width', this.settings.width);
      this.renderer.setStyle(this.elRef.nativeElement, 'min-width', this.settings.width);
    }
    if (this.settings.order) {
      this.renderer.setStyle(this.elRef.nativeElement, 'order', this.settings.order);
    }
    if (this.settings.flexBasis) {}
    switch (this.settings.content) {
      case 'numeric':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'img':
        // TODO
        break;
      case 'text':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-end');
        break;
      case 'array-text':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-end');
        break;
      case 'link':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'date':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'master':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-start');
        break;
      case 'money':
        this.renderer.setStyle(this.titleContainer.nativeElement, 'justify-content', 'flex-start');
        break;
    }
  }

  sort() {
    this.settings.sortDir === null ? this.settings.sortDir ='desc'
      : this.settings.sortDir === 'asc' ? this.settings.sortDir = 'desc' : this.settings.sortDir ='asc';
    this.events.emit({
      type: 'sort',
      value: {
        field: Object.keys(this.value)[0],
        type: this.settings.sortDir
        }
      });
  }

  ngOnInit() {
    this.apiName = Object.getOwnPropertyNames(this.value)[0];
    this.val = this.value[this.apiName];
    this.setStyles();
  }

}
