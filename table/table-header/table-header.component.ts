import {Component, EventEmitter, Input, OnChanges, SimpleChange, SimpleChanges} from '@angular/core';
import {ITableResponse} from '../../../interfaces/ITableResponse';
import {TableSettings} from '../../../models/TableSettings';

@Component({
  selector: 'app-table-header',
  templateUrl: './table-header.component.html',
  styleUrls: ['./table-header.component.scss']
})
export class TableHeaderComponent implements OnChanges {

  @Input() titles: Array<any>;
  @Input() settings: TableSettings;
  @Input() events: EventEmitter<ITableResponse>;
  cellSettings = [];
  constructor() { }

  makeCellSettings(settings) {
    this.cellSettings = [];
    this.titles.forEach(title => {
      const cellName = Object.getOwnPropertyNames(title)[0];
      this.cellSettings.push(
        {
          shadowed: settings.columns[cellName].shadowed,
          flexGrow: settings.flexGrow,
          width: settings.columns[cellName].width,
          flexBasis: settings.columns[cellName].flexBasis,
          filter: settings.columns[cellName].filter,
          sort: settings.columns[cellName].sort,
          sortDir: settings.columns[cellName].sortDir || null,
          content: settings.columns[cellName].content,
          order: settings.columns[cellName].order ? settings.columns[cellName].order : null,
        });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const settings: SimpleChange = changes.settings;
    settings ? this.makeCellSettings(settings.currentValue) : null;
  }
}
