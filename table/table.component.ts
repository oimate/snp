import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ITableResponse } from '../../interfaces/ITableResponse';
import { TableSettings } from '../../models/TableSettings';
import { ITableEssentials } from '../../interfaces/ITableEssentials';
import { ITableData } from '../../interfaces/ITableData';
import { ITableRowCellSettings } from './interfaces/ITableRowCellSettings';
import { ITablePaginationSettings } from './interfaces/ITablePaginationSettings';
import { IActionColumnSettings } from './interfaces/IActionColumnSettings';
import { ObjectIterators } from '../../models/ObjectIterators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  @Input() essentials$?: Observable<ITableEssentials>;
  @Output() events: EventEmitter<ITableResponse> = new EventEmitter<ITableResponse>();
  data: ITableData;
  settings: TableSettings;
  sub: Subscription;
  cellSettings = {
    titles: [],
    cells: [],
    actionColumn: false,
    flexGrow: false,
  } as ITableRowCellSettings;
  paginationSettings = {} as ITablePaginationSettings;
  actionSettings = {} as IActionColumnSettings;
  constructor() { }
  // TODO: header filter event emitters

  private getEssentials() {
    this.sub = this.essentials$.subscribe(essentials => {
      this.settings = essentials.settings;
      this.data = essentials.data;
      this.fCellSettings(essentials.data.titles);
      this.fPaginationSettings();
      this.cellSettings.titles = essentials.data.titles;
    });
  }

  private tableWidthStyles():Object {
    let st = {};
    if (this.settings.extraWidth) {
      st['width'] = `${this.settings.extraWidth}%`;
    }
    if (this.settings.tableMinWidth) {
      st['min-width'] = this.settings.tableMinWidth;
    }
    return st;
  }

  private fPaginationSettings() {
    this.paginationSettings = {
      perPage: this.settings.perPage,
      total: typeof this.settings.total === 'string' ? parseInt(this.settings.total, 10) : this.settings.total,
      currentPage: this.data.currentPage,
      perPages: this.settings.perPages,
      totalElements: (this.data.rows.length) ? this.data.rows.length : 0,
    };

  }

  private fCellSettings(titles) {
    this.cellSettings.cells = [];
    this.cellSettings.flexGrow = this.settings.flexGrow;
    titles.forEach(title => {
      const cellName = Object.getOwnPropertyNames(title)[0];
      this.cellSettings.cells.push(
        {
          routerLink: this.settings.columns[cellName].routerLink ? this.settings.columns[cellName].routerLink : undefined,
          shadowed: this.settings.columns[cellName].shadowed,
          content: this.settings.columns[cellName].content,
          flexBasis: this.settings.columns[cellName].flexBasis,
          width: this.settings.columns[cellName].width,
          events: this.settings.triggers && this.settings.triggers[cellName] ? this.settings.triggers[cellName] : undefined,
          order: this.settings.columns[cellName].order
        });
    });
    if (this.settings.actionColumn) { this.cellSettings.actionColumn = this.fActionSettings(); }
  }
  private fActionSettings() {
    if (this.settings.actionColumn) {
      this.actionSettings = {
        width: this.settings.actionColumn.width,
        flexBasis: this.settings.actionColumn.flexBasis,
        direction: this.settings.actionColumn.direction,
        actions: ObjectIterators.actionFields(this.settings.actionColumn.links, this.settings.actionColumn.triggers)
      };
    }
  }

  ngOnInit() {
    if (this.essentials$) {
      this.getEssentials();
    }
  }

  ngOnDestroy() {
    if (this.sub) { this.sub.unsubscribe(); }
  }

}
