import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableComponent} from './table.component';
import {TableHeaderComponent} from './table-header/table-header.component';
import {TableHeaderCellComponent} from './table-header/table-header-cell/table-header-cell.component';
import {TableRowComponent} from './table-row/table-row.component';
import {TablePaginationComponent} from './table-pagination/table-pagination.component';
import {TableRowCellComponent} from './table-row/table-row-cell/table-row-cell.component';
import {RouterModule} from '@angular/router';
import {BsDropdownModule} from 'ngx-bootstrap';
import {UiModule} from "../../UI/ui.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BsDropdownModule,
    UiModule,
  ],
  declarations: [
    TableComponent,
    TableHeaderComponent,
    TableHeaderCellComponent,
    TableRowComponent,
    TableRowCellComponent,
    TablePaginationComponent,
  ],
  exports: [
    TableComponent,
  ]
})
export class TableModule { }
