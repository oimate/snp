import {AfterViewInit, Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TwoSidesRadioDto} from './two-sides-radio-dto';

const RIGHT_TO_LEFT_DISPLACEMENT = 'r-t-l';
const LEFT_TO_RIGHT_DISPLACEMENT = 'l-t-r';

@Component({
  selector: 'control-two-sides-radio',
  template: `
    <div class="two-sides-radio">
      
      <label [ngClass]="{'action-label' : val == right.value}" 
             (click)="leftSideClick()">
        {{left.label}}</label>
      
      <div class="radio" (click)="switchValue()">
        <div class="dot" 
             [ngClass]="{'right-dot' : val == right.value, 'left-dot' : val == left.value,
                          'left-to-right-animated' : displacement === 'l-t-r',
                          'right-to-left-animated' : displacement === 'r-t-l'}"></div>
        <input class="inp" type="text" [(ngModel)]="value">
      </div>
      
      <label [ngClass]="{'action-label' : val == left.value}" 
             (click)="rightSideClick()">{{right.label}}</label>
    </div>
  `,
  styleUrls: ['./two-sides-radio.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TwoSidesRadioComponent),
      multi: true
    }
  ]
})
export class TwoSidesRadioComponent implements AfterViewInit, OnInit, ControlValueAccessor {

  @Input() left: TwoSidesRadioDto;
  @Input() right: TwoSidesRadioDto;
  val = '';
  displacement;


  // TODO: switch animations

  constructor() {
  }

  onChange = (value: string) => {};

  onTouched = () => {};

  set value(val: any) {
    if (val !== undefined && this.val !== val) {
      this.val = val;
      this.onChange(val);
      this.onTouched();
    }
  }

  writeValue(value: string): void {
    this.val = value;
    this.onChange(this.val);
  }

  switchValue() {
    if (this.val === this.right.value) {
      this.value = this.left.value;
      this.displacement = RIGHT_TO_LEFT_DISPLACEMENT;
      return;
    }
    if (this.val === this.left.value) {
      this.value = this.right.value;
      this.displacement = LEFT_TO_RIGHT_DISPLACEMENT;
      return;
    }
  }

  rightSideClick() {
    if (this.val !== this.right.value) {
      this.value = this.right.value;
      this.displacement = LEFT_TO_RIGHT_DISPLACEMENT;
    }
  }

  leftSideClick() {
    if (this.val !== this.left.value) {
      this.value = this.left.value;
      this.displacement = RIGHT_TO_LEFT_DISPLACEMENT;
    }
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.value = this.left.value;
    });
  }
}
