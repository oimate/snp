import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TwoSidesRadioComponent} from './two-sides-radio.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    TwoSidesRadioComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    TwoSidesRadioComponent
  ]
})
export class TwoSidesRadioModule { }
